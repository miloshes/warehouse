class UserSessionsController < ApplicationController
  def new
  end

  def create
    user = User.where(email: params[:email]).first

    if user && user.authenticate(params[:password])
      session[:user_id] = user.id

      if session[user.c_name]
        if user.active_connection && (user.active_connection.id != current_user_connection.id)
          user.active_connection.kill
        end

        current_user_connection.update_attributes(active: true, last_activity: DateTime.now)
        ConnectionLog.create(connection_id: current_user_connection.id, user_id: user.id, category: ConnectionLog::LOGGED_IN)
      else
        user.active_connection.kill if user.active_connection
        connection = user.connections.create(active: true, last_activity: DateTime.now)
        session[user.c_name] = connection.id
      end

      redirect_to items_path
    else
      flash[:alert] = 'Wrong credentials'
      render :new
    end
  end

  def destroy
    current_user_connection.update_attribute(:active, false)
    ConnectionLog.create(connection_id: current_user_connection.id, user_id: session[:user_id], category: ConnectionLog::LOGGED_OUT)
    session[:user_id] = nil

    redirect_to new_user_session_path, alert: "You've been successfuly logged out."
  end
end
