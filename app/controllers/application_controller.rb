class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :current_connection
  
  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    end
  end

  def current_user_connection
    if current_user
      @current_user_connection ||= Connection.find(session[current_user.c_name])
    end
  end

  def require_current_user
    if current_user
      # assuming current_connection always exists alongside current_user
      if current_user_connection.active?
        unless current_user_connection.expired?
          current_user_connection.update_attribute(:last_activity, DateTime.now)
        else
          ConnectionLog.create(connection_id: current_user_connection.id, user_id: current_user.id, category: ConnectionLog::EXPIRED)
          current_user_connection.update_attribute(:active, false)
          session[:user_id] = nil

          redirect_to new_user_session_path, alert: "You were automatically logged out due to inactivity. Login required"
        end
      else
        session[:user_id] = nil

        redirect_to new_user_session_path, alert: "Login on different machine detected. Login required"
      end
    else
      redirect_to new_user_session_path, alert: "Login required"
    end
  end

end
