class ItemsController < ApplicationController
  before_action :require_current_user

  def index
    if params[:sort]
      @items = Item.order(params[:sort])
    else
      @items = Item.all
    end
  end

  def new
    @item = Item.new
  end

  def create
    @item = Item.new item_params
    if @item.save
      redirect_to items_path
    else
      render :new
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])

    if @item.update item_params
      redirect_to items_path
    else
      render :edit
    end
  end

  def logs
    @versions = PaperTrail::Version.order('created_at DESC')
  end

  private

    def item_params
      params.require(:item).permit(:name, :description, :status, :critical_level, :category)
    end
end
