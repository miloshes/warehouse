class ConnectionLogsController < ApplicationController
  def index
    @connection_logs = ConnectionLog.order('created_at DESC')
  end
end
