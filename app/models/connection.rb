class Connection < ActiveRecord::Base
  belongs_to :user
  has_many :connection_logs

  after_create :log_connection

  SESSION_LENGTH = 1.minute

  scope :active, -> { where(active: true) }

  def expired?
    Time.now.minus_with_coercion(last_activity).round > SESSION_LENGTH
  end

  def kill
    if active == true
      update_attribute(:active, false)
      ConnectionLog.create(connection_id: id, user_id: user_id, category: ConnectionLog::KILLED)
    end
  end

  private

    def log_connection
      ConnectionLog.create(connection_id: id, user_id: user_id, category: ConnectionLog::LOGGED_IN)
    end
end
