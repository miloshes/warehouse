class User < ActiveRecord::Base
  has_secure_password

  has_many :connections
  has_many :connection_logs

  def c_name
    "connection_user_#{id}"
  end
  def active_connection
    connections.active.first
  end
end
