class Item < ActiveRecord::Base
  CHAIR = 1
  TABLE = 2
  WINDOW = 3
  DOOR = 4

  CATEGORIES = {
    1 => "chair",
    2 => "table",
    3 => "window",
    4 => "door"
  }

  has_paper_trail on: [:update], only: [:status]

  validates :name, :status, :critical_level, :category, presence: true

  validates :status, :critical_level, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  after_save :check_critical_level

  def category_name
    CATEGORIES[category]
  end

  private
    def check_critical_level
      if status <= critical_level
        AlertMailer.send_alert(self).deliver
      end
    end
end
