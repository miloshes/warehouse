class ConnectionLog < ActiveRecord::Base

  LOGGED_IN = 1
  LOGGED_OUT = 2
  EXPIRED = 3
  KILLED = 4

  CATEGORIES = {
    1 => 'logged in',
    2 => 'logged out',
    3 => 'expired',
    4 => 'killed'
  }

  belongs_to :connection
  belongs_to :user

  def category_name
    CATEGORIES[category]
  end
end
