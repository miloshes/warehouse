class AlertMailer < ActionMailer::Base
  default from: "from@example.com"

  def send_alert(item)
    @item = item
    mail(to: 'jozef.fulop@innovatrics.com', subject: "Item #{item.name} has reached critical level")
  end
end
