module ApplicationHelper
   def flash_message(name)
    if flash[name]
      content_tag :div, flash[name], class: "alert alert-danger"
    end
  end
end
