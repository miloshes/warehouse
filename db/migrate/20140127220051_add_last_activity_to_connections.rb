class AddLastActivityToConnections < ActiveRecord::Migration
  def change
    add_column :connections, :last_activity, :datetime
  end
end
