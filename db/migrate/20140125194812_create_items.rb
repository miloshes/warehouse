class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :description
      t.integer :status
      t.integer :critical_level
      t.integer :type

      t.timestamps
    end
  end
end
