class RenameTypeInConnectionLogs < ActiveRecord::Migration
  def change
    rename_column :connection_logs, :type, :category
  end
end
