class CreateConnections < ActiveRecord::Migration
  def change
    create_table :connections do |t|
      t.string :user_id
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
