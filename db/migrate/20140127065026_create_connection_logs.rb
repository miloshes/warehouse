class CreateConnectionLogs < ActiveRecord::Migration
  def change
    create_table :connection_logs do |t|
      t.integer :connection_id
      t.integer :user_id
      t.integer :type

      t.timestamps
    end
  end
end
